// import 'package:flutter/material.dart';
//
// import 'api_service.dart';
//
// class QueryInputScreen extends StatefulWidget {
//   @override
//   _QueryInputScreenState createState() => _QueryInputScreenState();
// }
//
// class _QueryInputScreenState extends State<QueryInputScreen> {
//   final TextEditingController _queryController = TextEditingController();
//   final ApiService _apiService = ApiService();
//   List<Document> _documents = [];
//   bool _isLoading = false;
//   String? _error;
//
//   void _search() async {
//     setState(() {
//       _isLoading = true;
//       _error = null;
//     });
//
//     try {
//       // Simulate network delay for testing
//       await Future.delayed(Duration(seconds: 2));
//
//       List<Document> results = await _apiService.searchDocuments(_queryController.text);
//       setState(() {
//         _documents = results;
//       });
//     } catch (e) {
//       setState(() {
//         _error = 'Failed to load search results';
//       });
//     } finally {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Query Input')),
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           children: [
//             TextField(
//               controller: _queryController,
//               decoration: InputDecoration(
//                 hintText: 'Enter your query',
//                 suffixIcon: IconButton(
//                   icon: Icon(Icons.search),
//                   onPressed: _search,
//                 ),
//               ),
//             ),
//             SizedBox(height: 20),
//             if (_isLoading)
//               Center(child: CircularProgressIndicator())
//             else if (_error != null)
//               Center(child: Text(_error!, style: TextStyle(color: Colors.red)))
//             else
//               Expanded(
//                 child: ListView.builder(
//                   itemCount: _documents.length,
//                   itemBuilder: (context, index) {
//                     return ListTile(
//                       title: Text(_documents[index].title),
//                       subtitle: Text(_documents[index].text),
//                       trailing: Text('Score: ${_documents[index].score}'),
//                     );
//                   },
//                 ),
//               ),
//           ],
//         ),
//       ),
//     );
//   }
// }

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'api_service.dart';

class QueryInputScreen extends StatefulWidget {
  @override
  _QueryInputScreenState createState() => _QueryInputScreenState();
}

class _QueryInputScreenState extends State<QueryInputScreen> {
  final TextEditingController _queryController = TextEditingController();
  final ApiService _apiService = ApiService();
  List<Document> _documents = [];
  bool _isLoading = false;
  String? _error;
  List<Map<String, dynamic>> queries = [];
  List<String> _suggestions = [];
  Future<void> sendFeedback(String docId, bool relevant, String feedback) async {
    try {
      final response = await http.post(
        Uri.parse('http://127.0.0.1:5000/feedback'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({'doc_id': docId, 'relevant': relevant, 'feedback': feedback}),
      );

      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Feedback sent successfully')),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Failed to send feedback')),
        );
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Error sending feedback')),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    fetchQueries();
    _queryController.addListener(_onQueryChanged);
  }

  void _onQueryChanged() {
    fetchSuggestions(_queryController.text);
  }

  Future<void> fetchQueries() async {
    try {
      final response = await http.get(Uri.parse('http://127.0.0.1:5000/queries'));
      if (response.statusCode == 200) {
        setState(() {
          queries = List<Map<String, dynamic>>.from(jsonDecode(response.body));
        });
      } else {
        throw Exception('Failed to load queries');
      }
    } catch (e) {
      setState(() {
        _error = 'Failed to load queries';
      });
    }
  }

  void fetchSuggestions(String query) {
    setState(() {
      if (query.isNotEmpty) {
        _suggestions = queries
            .where((q) => q['text']!.toLowerCase().contains(query.toLowerCase()))
            .map<String>((q) => q['text'] as String)
            .toList();
      } else {
        _suggestions = [];
      }
    });
  }

  void _search() async {
    setState(() {
      _isLoading = true;
      _error = null;
    });

    try {
      // Simulate network delay for testing
      await Future.delayed(Duration(seconds: 2));

      List<Document> results = await _apiService.searchDocuments(_queryController.text);
      setState(() {
        _documents = results;
      });
    } catch (e) {
      setState(() {
        _error = 'Failed to load search results';
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Query Input')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: _queryController,
              decoration: InputDecoration(
                hintText: 'Enter your query',
                suffixIcon: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: _search,
                ),
              ),
            ),
            SizedBox(height: 10),
            if (_suggestions.isNotEmpty)
              Container(
                height: 100,
                child: ListView.builder(
                  itemCount: _suggestions.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(_suggestions[index]),
                      onTap: () {
                        setState(() {
                          _queryController.text = _suggestions[index];
                          _suggestions.clear();
                        });
                      },
                    );
                  },
                ),
              ),
            SizedBox(height: 20),
            if (_isLoading)
              Center(child: CircularProgressIndicator())
            else if (_error != null)
              Center(child: Text(_error!, style: TextStyle(color: Colors.red)))
            else
              Expanded(
                child: ListView.builder(
                  itemCount: _documents.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(_documents[index].title),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(_documents[index].text),
                          SizedBox(height: 10),
                          Row(
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  sendFeedback(_documents[index].id, true, 'Relevant');
                                },
                                child: Text('Relevant'),
                              ),
                              SizedBox(width: 10),
                              ElevatedButton(
                                onPressed: () {
                                  sendFeedback(_documents[index].id, false, 'Not Relevant');
                                },
                                child: Text('Not Relevant'),
                              ),
                            ],
                          ),
                        ],
                      ),
                      trailing: Text('Score: ${_documents[index].score}'),
                    );
                  },
                ),
              ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _queryController.removeListener(_onQueryChanged);
    _queryController.dispose();
    super.dispose();
  }
}

// class Document {
//   final String title;
//   final String text;
//   final double score;
//
//   Document({required this.title, required this.text, required this.score});
// }
//
// // Dummy ApiService class
// class ApiService {
//   Future<List<Document>> searchDocuments(String query) async {
//     // Implement your API call here and return the list of documents
//     return [
//       Document(title: 'Title 1', text: 'Text 1', score: 0.9),
//       Document(title: 'Title 2', text: 'Text 2', score: 0.8),
//       // Add more documents as needed
//     ];
//   }
// }
