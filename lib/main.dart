import 'package:flutter/material.dart';
import 'package:ir_system_frontend/file_query_screen.dart';
import 'package:ir_system_frontend/query_input_screen.dart';

import 'evaluation_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'IR Project',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
          primarySwatch: Colors.indigo,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        // home: const MyHomePage(title: 'My IR System'),
        home: HomeScreen());
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('IR Project')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => QueryInputScreen()),
                );
              },
              child: Text('Write Query'),
            ),
            SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EvaluationScreen()),
                );
              },
              child: Text('Select Predefined Query'),
            ),
            SizedBox(
              height: 30,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FileQueryScreen()),
                );
              },
              child: Text('Select Files To Query'),
            ),
          ],
        ),
      ),
    );
  }
}
// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }
//
// class _HomeScreenState extends State<HomeScreen> {
//   TextEditingController _searchController = TextEditingController();
//   List<String> _searchResults = [];
//   List<String> _searchHistory = [];
//   bool _isLoading = false;
//   final ApiService _apiService = ApiService();
//   List<Document> _searchResultsDocument = [];
//
//   // bool _isLoading = false;
//   String? _error;
//   Map<String, double>? _evaluationMetrics;
//
//   void _performSearch(String text) async {
//     setState(() {
//       _isLoading = true;
//       _error = null;
//       _evaluationMetrics = null;
//     });
//
//     try {
//       List<Document> results =
//           await _apiService.searchDocuments(_searchController.text);
//       setState(() {
//         _searchResultsDocument = results;
//       });
//
//       // Perform evaluation (using mock data for demonstration)
//       Map<String, dynamic> evaluationData = {
//         'query_id': 'example_query_id',
//         'retrieved_docs': results.map((doc) => doc.id).toList(),
//         'qrels': {
//           'example_query_id': ['relevant_doc_id']
//         }
//       };
//
//       Map<String, double> evaluationResults =
//           await _apiService.evaluateSystem(evaluationData);
//       setState(() {
//         _evaluationMetrics = evaluationResults;
//       });
//     } catch (e) {
//       setState(() {
//         _error = 'Failed to load search results';
//       });
//     } finally {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _loadSearchHistory();
//   }
//
//   void _loadSearchHistory() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     setState(() {
//       _searchHistory = prefs.getStringList('searchHistory') ?? [];
//     });
//   }
//
//   void _saveSearchHistory(String query) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     _searchHistory.add(query);
//     await prefs.setStringList('searchHistory', _searchHistory);
//   }
//
//   void _performSearch1(String query) {
//     setState(() {
//       _isLoading = true;
//     });
//     // Simulate network call
//     Future.delayed(Duration(seconds: 2), () {
//       setState(() {
//         _isLoading = false;
//         _searchResults = ['Result 1', 'Result 2', 'Result 3']; // Mock results
//       });
//     });
//   }
//
//   // void _performSearch(String query) {
//   //    setState(() {
//   //      _isLoading=true;
//   //    });
//   //   // Your search logic here, e.g., querying a database or API
//   //    Future.delayed(Duration(seconds: 2), () {
//   //      setState(() {
//   //        _isLoading=false;
//   //        _searchResults =
//   //            searchDatabase(query); // Replace with actual search logic
//   //      });
//   //    });
//   //
//   // }
//
//   List<String> searchDatabase(String query) {
//     // Placeholder for actual search implementation
//     return ['Result 1 for $query', 'Result 2 for $query'];
//   }
//
//   void _performSearch2() async {
//     setState(() {
//       _isLoading = true;
//       _error = null;
//     });
//
//     try {
//       List<Document> results =
//           await _apiService.searchDocuments(_searchController.text);
//       setState(() {
//         _searchResultsDocument = results;
//       });
//     } catch (e) {
//       setState(() {
//         _error = 'Failed to load search results';
//       });
//     } finally {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }
//
//   void _getAllDocuments() async {
//     setState(() {
//       _isLoading = true;
//       _error = null;
//     });
//
//     try {
//       List<Document> documents = await _apiService.getAllDocuments();
//       setState(() {
//         _searchResultsDocument = documents;
//       });
//     } catch (e) {
//       setState(() {
//         _error = 'Failed to load documents';
//       });
//     } finally {
//       setState(() {
//         _isLoading = false;
//       });
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(title: Text('Information Retrieval')),
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           children: [
//             TextField(
//               controller: _searchController,
//               decoration: InputDecoration(
//                 focusedBorder: OutlineInputBorder(),
//                 enabledBorder: OutlineInputBorder(),
//                 hintText: 'Enter search query',
//                 suffixIcon: IconButton(
//                   icon: Icon(Icons.search),
//                   onPressed: () => _performSearch(_searchController.text),
//                 ),
//               ),
//             ),
//             SizedBox(height: 20),
//             Text('Search History:'),
//             Expanded(
//               child: ListView.builder(
//                 itemCount: _searchHistory.length,
//                 itemBuilder: (context, index) {
//                   return ListTile(
//                     title: Text(_searchHistory[index]),
//                     onTap: () {
//                       _searchController.text = _searchHistory[index];
//                       _performSearch(_searchHistory[index]);
//                     },
//                   );
//                 },
//               ),
//             ),
//             SizedBox(height: 20),
//             // Text('Search Results:'),
//             // _isLoading
//             //     ? Center(child: SpinKitFadingCircle(color: Colors.blue))
//             //     :
//             // Expanded(
//             //   child: ListView.builder(
//             //     itemCount: _searchResults.length,
//             //     itemBuilder: (context, index) {
//             //       return ListTile(
//             //         title: Text(_searchResults[index]),
//             //       );
//             //     },
//             //   ),
//             // ),
//             ElevatedButton(
//               onPressed: _getAllDocuments,
//               child: Text('Get All Documents'),
//             ),
//             SizedBox(height: 20),
//             if (_isLoading)
//               Center(child: CircularProgressIndicator())
//             else if (_error != null)
//               Text(_error!, style: TextStyle(color: Colors.red))
//             else ...[
//               if (_evaluationMetrics != null) ...[
//                 Text('Evaluation Metrics:',
//                     style: TextStyle(fontWeight: FontWeight.bold)),
//                 Text('Recall: ${_evaluationMetrics!['Recall']}'),
//                 Text('MAP: ${_evaluationMetrics!['MAP']}'),
//                 Text('Precision: ${_evaluationMetrics!['Precision']}'),
//                 Text('MRR: ${_evaluationMetrics!['MRR']}'),
//                 SizedBox(height: 20),
//               ],
//               Expanded(
//                 child: ListView.builder(
//                   itemCount: _searchResults.length,
//                   itemBuilder: (context, index) {
//                     return Card(
//                       margin: EdgeInsets.symmetric(vertical: 5.0),
//                       child: ListTile(
//                         title: Text(_searchResultsDocument[index].title),
//                         subtitle: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Text('ID: ${_searchResultsDocument[index].id}'),
//                             Text(
//                                 'Score: ${_searchResultsDocument[index].score}'),
//                             Text('Text: ${_searchResultsDocument[index].text}',
//                                 maxLines: 3, overflow: TextOverflow.ellipsis),
//                           ],
//                         ),
//                       ),
//                     );
//                   },
//                 ),
//               ),
//             ],
//           ],
//         ),
//       ),
//     );
//   }
// }

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the other colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(
                  labelText: "write a query",
                  focusedBorder: OutlineInputBorder(),
                  enabledBorder: OutlineInputBorder()),
            ),
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: _incrementCounter,
            tooltip: 'Search',
            child: const Icon(Icons.search),
          ),
          SizedBox(
            width: 20,
          ),
          FloatingActionButton(
            onPressed: _incrementCounter,
            tooltip: 'Select files to search in',
            child: const Icon(Icons.select_all),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
