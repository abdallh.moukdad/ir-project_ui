import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'api_service.dart';

Future<Map<String, dynamic>> evaluateQuery(String queryId, String queryText,
    {int k = 10}) async {
  final response = await http.post(
    Uri.parse('http://127.0.0.1:5000/evaluate'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, dynamic>{
      'query_id': queryId,
      'query_text': queryText,
      'k': k,
    }),
  );

  if (response.statusCode == 200) {
    print(response.body);
    return jsonDecode(response.body);
  } else {
    throw Exception('Failed to load evaluation results');
  }
}

Future<List<dynamic>> getQueries() async {
  final response = await http.get(
    Uri.parse('http://127.0.0.1:5000/queries'),
  );

  if (response.statusCode == 200) {
    return jsonDecode(response.body);
  } else {
    throw Exception('Failed to load queries');
  }
}

class EvaluationScreen extends StatefulWidget {
  @override
  _EvaluationScreenState createState() => _EvaluationScreenState();
}

class _EvaluationScreenState extends State<EvaluationScreen> {
  Future<Map<String, dynamic>>? _evaluationResults;
  late Future<List<dynamic>> _queries;

  @override
  void initState() {
    super.initState();
    _queries = getQueries();
  }

  void _evaluateQuery(String queryId, String queryText) {
    setState(() {
      _evaluationResults = evaluateQuery(queryId, queryText);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Evaluation Screen'),
      ),
      body: FutureBuilder<List<dynamic>>(
        future: _queries,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: CircularProgressIndicator());
          } else if (snapshot.hasError) {
            return Center(child: Text('Error: ${snapshot.error}'));
          } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
            return Center(child: Text('No queries available'));
          }

          final queries = snapshot.data!;

          return ListView.builder(
            itemCount: queries.length,
            itemBuilder: (context, index) {
              print(queries);
              final query = queries[index];
              return ListTile(
                title: Text(query['text']),
                subtitle: Text(query['_id']),
                onTap: () => _evaluateQuery(query['_id'], query['text']),
              );
            },
          );
        },
      ),
      floatingActionButton: _evaluationResults != null
          ? FutureBuilder<Map<String, dynamic>>(
              future: _evaluationResults,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return FloatingActionButton(
                    onPressed: null,
                    child: CircularProgressIndicator(),
                  );
                } else if (snapshot.hasError) {
                  return FloatingActionButton(
                    onPressed: null,
                    child: Icon(Icons.error),
                  );
                } else if (snapshot.hasData) {
                  final evaluationMetrics =
                      EvaluationMetrics.fromJson(snapshot.data!['evaluation']);
                  final documents = (snapshot.data!['documents'] as List)
                      .map((doc) => Document.fromJson(doc))
                      .toList();

                  return FloatingActionButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return Dialog(
                            insetPadding: EdgeInsets.all(16.0), // Control the padding around the modal
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.9, // Set width to 90% of screen width
                              height: MediaQuery.of(context).size.height * 0.9, // Set height to 90% of screen height
                              padding: EdgeInsets.all(16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Evaluation Metrics', style: Theme.of(context).textTheme.headline6),
                                  SizedBox(height: 8.0),
                                  Text('Precision@k: ${evaluationMetrics.precisionAtK}'),
                                  Text('Recall: ${evaluationMetrics.recall}'),
                                  Text('MAP: ${evaluationMetrics.map}'),
                                  Text('MRR: ${evaluationMetrics.mrr}'),
                                  SizedBox(height: 16.0),
                                  Text('Documents:', style: Theme.of(context).textTheme.subtitle1),
                                  Expanded(
                                    child: ListView.builder(
                                      itemCount: documents.length,
                                      itemBuilder: (context, index) {
                                        final document = documents[index];
                                        return ListTile(
                                          title: Text(document.title),
                                          subtitle: Text(document.text),
                                          trailing: Text(document.score.toString()),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                    child: Icon(Icons.assessment),
                  );

                } else {
                  return FloatingActionButton(
                    onPressed: null,
                    child: Icon(Icons.error_outline),
                  );
                }
              },
            )
          : null,
    );
  }
}
