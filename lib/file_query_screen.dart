import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'api_service.dart';

class FileQueryScreen extends StatefulWidget {
  @override
  _FileQueryScreenState createState() => _FileQueryScreenState();
}

class _FileQueryScreenState extends State<FileQueryScreen> {
  // Map<String, String> files = {};
  List<Document>files=[];
  Set<String> selectedFiles = {};
  bool isLoading = true;
  TextEditingController queryController = TextEditingController();

  @override
  void initState() {
    super.initState();
    fetchFiles();
  }

  List<Document> parseDebates(String jsonData) {
    final parsed = json.decode(jsonData).cast<Map<String, dynamic>>();
    return parsed.map<Document>((json) => Document.fromJson(json)).toList();
  }

  Future<void> fetchFiles() async {
    final response = await http.get(Uri.parse('http://127.0.0.1:5000/files'));
    if (response.statusCode == 200) {
      setState(() {
        print(response.body.runtimeType);
        // files = Map<String, String>.from(json.decode(response.body));
        files = parseDebates(response.body);
        isLoading = false;
      });
    } else {
      throw Exception('Failed to load files');
    }
  }

  Future<void> evaluateQuery() async {
    final response = await http.post(
      Uri.parse('http://127.0.0.1:5000/search/selected'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        'query_text': queryController.text,
        'file_ids': selectedFiles.toList(),
        'k': 10,
      }),
    );
    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EvaluationScreenFiles(data: data),
        ),
      );
    } else {
      throw Exception('Failed to evaluate query');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select Files and Enter Query'),
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: files.length,
                    itemBuilder: (context, index) {
                      final file=files[index];

                      // String fileId = files.keys.elementAt(index);
                      // String fileName = files[fileId]!;
                      return CheckboxListTile(
                        title: Text(file.title),
                        value: selectedFiles.contains(file.id),
                        onChanged: (bool? value) {
                          setState(() {
                            if (value == true) {
                              selectedFiles.add(file.id);
                            } else {
                              selectedFiles.remove(file.id);
                            }
                          });
                        },
                      );
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: queryController,
                    decoration: InputDecoration(
                      labelText: 'Enter your query',
                      border: OutlineInputBorder(),
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: evaluateQuery,
                  child: Text('Search'),
                ),
              ],
            ),
    );
  }
}

class EvaluationScreenFiles extends StatelessWidget {
  final Map<String, dynamic> data;

  EvaluationScreenFiles({required this.data});

  @override
  Widget build(BuildContext context) {
    // final metrics = data['evaluation_metrics'];
    final docs = data['retrieved_docs'];

    return Scaffold(
      appBar: AppBar(
        title: Text('Evaluation Results'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text('Evaluation Metrics',
            //     style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            // Text('Precision: ${metrics['precision']}'),
            // Text('Recall: ${metrics['recall']}'),
            // Text('MAP: ${metrics['MAP']}'),
            // Text('MRR: ${metrics['MRR']}'),
            SizedBox(height: 20),
            Text('Retrieved Documents',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
            ...docs.map<Widget>((doc) {
              return ListTile(
                title: Text(doc['title']),
                subtitle: Text(doc['text']),
                trailing: Text('Score: ${doc['score']}'),
              );
            }).toList(),
          ],
        ),
      ),
    );
  }
}
