import 'package:flutter/material.dart';

import 'api_service.dart';

class QuerySelectionScreen extends StatefulWidget {
  @override
  _QuerySelectionScreenState createState() => _QuerySelectionScreenState();
}

class _QuerySelectionScreenState extends State<QuerySelectionScreen> {
  final ApiServiceMock _apiService = ApiServiceMock();
  List<Query> _queries = [];
  List<Document> _documents = [];
  bool _isLoading = false;
  String? _error;
  Map<String, double>? _evaluationMetrics;

  @override
  void initState() {
    super.initState();
    _fetchQueries();
  }

  void _fetchQueries() async {
    setState(() {
      _isLoading = true;
      _error = null;
    });

    try {
      List<Query> queries = await _apiService.getQueries();
      setState(() {
        _queries = queries;
      });
    } catch (e) {
      setState(() {
        _error = 'Failed to load queries';
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  void _evaluateQuery(String queryId, String queryText) async {
    setState(() {
      _isLoading = true;
      _error = null;
      _evaluationMetrics = null;
    });

    try {
      List<Document> results = await _apiService.searchDocuments(queryText);
      setState(() {
        _documents = results;
      });

      Map<String, dynamic> evaluationData = {
        'query_id': queryId,
        'retrieved_docs': results.map((doc) => doc.id).toList(),
        'qrels': {'example_query_id': ['relevant_doc_id']}  // Replace with real qrels
      };

      Map<String, double> evaluationResults = await _apiService.evaluateSystem(evaluationData);
      setState(() {
        _evaluationMetrics = evaluationResults;
      });
    } catch (e) {
      setState(() {
        _error = 'Failed to load search results';
      });
    } finally {
      setState(() {
        _isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Select Query')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            if (_isLoading)
              Center(child: CircularProgressIndicator())
            else if (_error != null)
              Center(child: Text(_error!, style: TextStyle(color: Colors.red)))
            else ...[
                Expanded(
                  child: ListView.builder(
                    itemCount: _queries.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(_queries[index].text),
                        onTap: () => _evaluateQuery(_queries[index].id, _queries[index].text),
                      );
                    },
                  ),
                ),
                if (_evaluationMetrics != null) ...[
                  Text('Evaluation Metrics:', style: TextStyle(fontWeight: FontWeight.bold)),
                  Text('Recall: ${_evaluationMetrics!['Recall']}'),
                  Text('MAP: ${_evaluationMetrics!['MAP']}'),
                  Text('Precision: ${_evaluationMetrics!['Precision']}'),
                  Text('MRR: ${_evaluationMetrics!['MRR']}'),
                  SizedBox(height: 20),
                ],
                Expanded(
                  child: ListView.builder(
                    itemCount: _documents.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(_documents[index].title),
                        subtitle: Text(_documents[index].text),
                        trailing: Text('Score: ${_documents[index].score}'),
                      );
                    },
                  ),
                ),
              ],
          ],
        ),
      ),
    );
  }
}
