import 'dart:convert';

import 'package:http/http.dart' as http;
class Query {
  final String id;
  final String text;

  Query({required this.id, required this.text});

  factory Query.fromJson(Map<String, dynamic> json) {
    return Query(
      id: json['id'],
      text: json['text'],
    );
  }
}
class EvaluationMetrics {
  final double precisionAtK;
  final double recall;
  final double map;
  final double mrr;

  EvaluationMetrics({
    required this.precisionAtK,
    required this.recall,
    required this.map,
    required this.mrr,
  });

  factory EvaluationMetrics.fromJson(Map<String, dynamic> json) {
    return EvaluationMetrics(
      precisionAtK: json['precision_at_k'],
      recall: json['recall'],
      map: json['MAP'],
      mrr: json['MRR']as double,
    );
  }
}

class Document {
  final String id;
  final String title;
  final String text;
  final double score;

  Document({
    required this.id,
    required this.title,
    required this.text,
    required this.score,
  });

  factory Document.fromJson(Map<String, dynamic> json) {
    return Document(
      id: json['_id'],
      title: json['title'],
      text: json['text'],
      score: json['score']??0,
    );
  }
}

class ApiService {
  static const String baseUrl = 'http://127.0.0.1:5000';

  // Get all documents
  Future<List<Document>> getAllDocuments() async {
    final response = await http.get(Uri.parse('$baseUrl/documents'));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<Document> documents =
          body.map((dynamic item) => Document.fromJson(item)).toList();
      return documents;
    } else {
      throw Exception('Failed to load documents');
    }
  }

  // Search documents
  Future<List<Document>> searchDocuments(String query) async {
    final response = await http.post(
      Uri.parse('$baseUrl/search/all'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode({'query': query}),
    );
    print("this the response:${response.body}");

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<Document> documents =
          body.map((dynamic item) => Document.fromJson(item)).toList();
      return documents;
    } else {
      print(response);
      throw Exception('Failed to load search results');
    }
  }

  // Get document by ID
  Future<Document> getDocumentById(String docId) async {
    final response = await http.get(Uri.parse('$baseUrl/document/$docId'));

    if (response.statusCode == 200) {
      return Document.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load document');
    }
  }

  // Add a new document
  Future<void> addDocument(Map<String, dynamic> document) async {
    final response = await http.post(
      Uri.parse('$baseUrl/document'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(document),
    );

    if (response.statusCode != 200) {
      throw Exception('Failed to add document');
    }
  }

  // Delete a document
  Future<void> deleteDocument(String docId) async {
    final response = await http.delete(Uri.parse('$baseUrl/document/$docId'));

    if (response.statusCode != 200) {
      throw Exception('Failed to delete document');
    }
  }

  // Update a document
  Future<void> updateDocument(
      String docId, Map<String, dynamic> document) async {
    final response = await http.put(
      Uri.parse('$baseUrl/document/$docId'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(document),
    );

    if (response.statusCode != 200) {
      throw Exception('Failed to update document');
    }
  }

  // Reindex documents
  Future<void> reindexDocuments() async {
    final response = await http.post(Uri.parse('$baseUrl/reindex'));

    if (response.statusCode != 200) {
      throw Exception('Failed to reindex documents');
    }
  }

  // Clear index
  Future<void> clearIndex() async {
    final response = await http.post(Uri.parse('$baseUrl/clear-index'));

    if (response.statusCode != 200) {
      throw Exception('Failed to clear index');
    }
  }

  // Evaluate system
  Future<Map<String, double>> evaluateSystem(
      Map<String, dynamic> evaluationData) async {
    final response = await http.post(
      Uri.parse('$baseUrl/evaluate'),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(evaluationData),
    );

    if (response.statusCode == 200) {
      return Map<String, double>.from(jsonDecode(response.body));
    } else {
      throw Exception('Failed to evaluate system');
    }
  }
  Future<List<Query>> getQueries() async {
    final response = await http.get(Uri.parse('$baseUrl/queries'));

    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      List<Query> queries = body.map((dynamic item) => Query.fromJson(item)).toList();
      return queries;
    } else {
      throw Exception('Failed to load queries');
    }
  }

}
class ApiServiceMock {
  Future<List<Query>> getQueries() async {
    // Simulating network delay
    await Future.delayed(Duration(seconds: 2));

    // Return dummy data for queries
    return [
      Query(id: '1', text: 'Example query 1'),
      Query(id: '2', text: 'Example query 2'),
      Query(id: '3', text: 'Example query 3'),
    ];
  }

  Future<List<Document>> searchDocuments(String queryText) async {
    // Simulating network delay
    await Future.delayed(Duration(seconds: 2));

    // Return dummy data for documents
    return [
      Document(id: 'doc1', title: 'Document 1', text: 'This is the text of document 1', score: 0.9),
      Document(id: 'doc2', title: 'Document 2', text: 'This is the text of document 2', score: 0.75),
      Document(id: 'doc3', title: 'Document 3', text: 'This is the text of document 3', score: 0.6),
    ];
  }

  Future<Map<String, double>> evaluateSystem(Map<String, dynamic> evaluationData) async {
    // Simulating network delay
    await Future.delayed(Duration(seconds: 2));

    // Return dummy data for evaluation metrics
    return {
      'Recall': 0.85,
      'MAP': 0.75,
      'Precision': 0.8,
      'MRR': 0.9,
    };
  }
}